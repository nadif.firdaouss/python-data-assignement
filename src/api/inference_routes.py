from flask import Blueprint, request
from src.constants import AGGREGATOR_MODEL_PATH
from src.models.aggregator_model import AggregatorModel
import numpy as np

model = AggregatorModel()
model.load(AGGREGATOR_MODEL_PATH)
blueprint = Blueprint('api', __name__, url_prefix='/api')

@blueprint.route('/')
@blueprint.route('/index')
def index():
    return "CARD FRAUD DETECTION API - INFERENCE BLUEPRINT"

@blueprint.route('/inference', methods=['POST'])
def run_inference():
    from src.api import PredictionsHistory, db
    if request.method == 'POST':
        features = np.array(request.json).reshape(1, -1)
        prediction = model.predict(features)
        target=str(prediction[0])
        features = [str(x) for x in features[0]]
        transaction = PredictionsHistory(features=features, status=target)
        db.session.add(transaction)
        db.session.commit()
        return target

@blueprint.route('/save', methods=['GET'])
def save():
    from src.api import PredictionsHistory
    if  request.method == 'GET':
        History_Predictions = PredictionsHistory.query.all()
        return str([{"feature11": History_Predictions.feature11, "feature13": History_Predictions.feature13, "feature15": History_Predictions.feature15, 
                    "transaction": History_Predictions.transaction, "prediction": History_Predictions.status} for History_Predictions in History_Predictions])



