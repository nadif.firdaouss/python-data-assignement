from flask import Flask

from src.api.index_routes import blueprint as index_blueprint
from src.api.inference_routes import blueprint as inference_blueprint
from os import path


app = Flask(__name__)
app.register_blueprint(index_blueprint)
app.register_blueprint(inference_blueprint)

from flask_sqlalchemy import SQLAlchemy

DB_NAME = "fraud_detection.db"
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True 
app.config['SQLALCHEMY_DATABASE_URI'] = f'sqlite:///{DB_NAME}'
db = SQLAlchemy(app)

class PredictionsHistory(db.Model):
    __tablename__ = "predictions_history"
    id = db.Column(db.Integer, primary_key=True)
    feature11 = db.Column(db.String(10))
    feature13 = db.Column(db.String(10))
    feature15 = db.Column(db.String(10))
    transaction = db.Column(db.String(10))
    status = db.Column(db.String(10))
    
    def __init__(self, features, status):
        self.feature11 = features[11]
        self.feature13 = features[13]
        self.feature15 = features[15]
        self.transaction = features[28]
        self.status=status

if not path.exists('/'+ DB_NAME):
    db.create_all()
db.session.commit()

if __name__ == "__main__":
    app.run()
