import streamlit as st
from PIL import Image

from src.constants import CM_PLOT_PATH
from src.training.train_pipeline import TrainingPipeline

def Training_section():
    st.header("Model Training")
    st.info("Before you proceed to training your model. Make sure you "
            "have checked your training pipeline code and that it is set properly.")

    modelName = st.multiselect(
     'Which models to use',
     ['randomforest', 'decisiontree', 'svc'])

    st.write('You selected:', modelName)

    
    serialize = st.checkbox('Save model')
    trainButton = st.button('Train Model')

    if trainButton:
        with st.spinner('Training model, please wait...'):
            try:
                trainPip = TrainingPipeline()
                trainPip.train(serialize=serialize, model_name=modelName)
                trainPip.render_confusion_matrix(plot_name="".join(modelName))
                accuracy, f1 = trainPip.get_model_perfomance()
                col1, col2 = st.columns(2)

                col1.metric(label="Accuracy score", value=str(round(accuracy, 4)))
                col2.metric(label="F1 score", value=str(round(f1, 4)))
                #need to add matrix code here
                st.image(Image.open(CM_PLOT_PATH))

            except Exception as e:
                st.error('Failed to train model!')
                st.exception(e)