import streamlit as st
import pandas as pd
import matplotlib.pyplot as plt
import plotly.express as px
import seaborn as sns
import numpy as np


def EDA_section(df):
    st.header("Exploratory Data Analysis")
    st.info("In this section, you are invited to create insightful graphs "
            "about the card fraud dataset that you were provided.")


    st.header("1. Dataset")
    st.dataframe(df.head())
    st.markdown("► Our dataset contains 284807 row and 31 column : ")
    st.code(df.shape)



    col1, col2= st.columns(2)

    with col1:
            #Bar Chart
        st.header("2. Bar Chart of Class column")
        fig = px.histogram(df, x="Class",color="Class")
        st.plotly_chart(fig, use_container_width=True)
    with col2:
        st.header("3. Pie chart of Class column")
            
        names = ['Clear', 'Fraudulent']
        fig = px.pie(values=df["Class"].value_counts(), names=names)
            # Plot!
        st.plotly_chart(fig, use_container_width=True)
    st.markdown("► This dataset has 492 fraudulent transactions and  284 315 clear transactions, equivalent to 0,173% fraudulent and 99,8% clear. Therefore, we can classify this dataset as highly unbalanced.")

    col1, col2= st.columns(2)
    with col1:
        st.header("4. Correlation Matrix Plot")

        fig, ax = plt.subplots(figsize=(11, 12))
        corr=df.corr()
            # Generate a mask for the upper triangle
        mask = np.triu(np.ones_like(corr, dtype=bool))
        sns.heatmap(corr,mask=mask, cmap="YlGnBu",vmin=-1, vmax=1,linewidths=.01,
                        square=True) # Displaying the Heatmap

        st.write(fig)

        st.markdown("► There is some few correlations between some of the features such as Time Column which has a negative correlation with V3 and Amount Column which has a positive correlation with V7 and V20, and a negative correlation with V1 and V5.")
        st.markdown("► Overall, low dependency between variables is due to PCA dimension reduction applied on our dataset")
    with col2:
        
        fig, ax = plt.subplots(figsize=(4, 5.5))
        corr = df.corrwith(df['Class']).reset_index()
        corr.columns = ['Index','Correlations']
        corr = corr.set_index('Index')
        corr = corr.sort_values(by=['Correlations'], ascending = False)
        st.header("5. Features Correlating with Class")
        heatmap = sns.heatmap(corr, 
                                vmin=-1, vmax=1, annot=True, cmap='Set3', 
                                linewidths=0.004, linecolor='white')
        
        st.write(fig)
        st.markdown("► Notice that V10, V12, V14 and V10 have a negative correlations which means the lower these values, the more likely to have a Fraudulent transaction.")
        st.markdown("► On the other hand, V19, V11, V4 and V2 have a positive correlation wich means the higher these values, the more likely to have a Fraudulent transaction.")
       
    
    st.header("6. Correlation Matrix Table")
    st.dataframe(df.corr())

    st.header("7. Describe table")
    st.dataframe(df.describe())
    st.markdown("► Description of statistic features (Sum, Average, Variance, minimum, 1st quartile, 2nd quartile, 3rd Quartile and Maximum)")

