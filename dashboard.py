import streamlit as st
import pandas as pd
from src.constants import DATASET_PATH
from src.dashboard_sections.eda_section import EDA_section
from src.dashboard_sections.training_section import Training_section
from src.dashboard_sections.inference_section import Inference_section

# Use the full page instead of a narrow central column
st.set_page_config(layout="wide")

@st.cache()
def load_data():
    df = pd.read_csv(
        DATASET_PATH
    )
    return df

# loading our dataset
df = load_data()

st.title("Card Fraud Detection Dashboard")
st.sidebar.title("Data Themes")

#side bar for selecting the interface
sidebar_options = st.sidebar.selectbox(
    "Options",
    ("EDA", "Training", "Inference")
)

#if selected name is EDA, load EDA class from dashboard_sections
if sidebar_options == "EDA": 
    EDA_section(df)
#else if selected name is Training, load Training class from dashboard_sections
elif sidebar_options == "Training": 
    Training_section()

#else if selected name is Inference, load Inference class from dashboard_sections
elif sidebar_options == "Inference": 
    Inference_section()